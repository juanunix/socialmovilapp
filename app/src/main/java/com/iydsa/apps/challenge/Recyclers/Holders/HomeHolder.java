package com.iydsa.apps.challenge.Recyclers.Holders;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeHolder extends RecyclerView.ViewHolder {

    public CircleImageView foto;
    public TextView usuario;
    public TextView lugar;
    public RecyclerView fotos;

    public HomeHolder(View itemView) {
        super(itemView);
        foto = itemView.findViewById(R.id.foto);
        usuario = itemView.findViewById(R.id.user);
        lugar = itemView.findViewById(R.id.place);
        fotos = itemView.findViewById(R.id.fotos);
    }
}
