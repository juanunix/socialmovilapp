package com.iydsa.apps.challenge.Recyclers.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.iydsa.apps.challenge.R;
import com.iydsa.apps.challenge.Recyclers.Clases.HomeClass;
import com.iydsa.apps.challenge.Recyclers.Holders.HomeHolder;

import java.util.ArrayList;

import Libraryes.Utils;

public class Home_adapter extends RecyclerView.Adapter<HomeHolder> {

    private Context context;
    private ArrayList<HomeClass> home;
    private Utils utils;
    private HomeImageAdapter homeImageAdapter;

    public Home_adapter(Context ctx, ArrayList<HomeClass> stories) {
        this.context = ctx;
        this.home = stories;
        utils = Utils.getInstance(ctx);
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_design, null);
        return new HomeHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {
        final HomeClass datos = home.get(position);
        if (position == 0) {
            utils.LoadImageUrl(datos.getImageUrl(), holder.foto);
        } else {
            utils.LoadImage(datos.getImage(), holder.foto);
        }
        holder.usuario.setText(datos.getUser());
        holder.lugar.setText(datos.getPlace());
        homeImageAdapter = new HomeImageAdapter(context, datos.getImagenes());
        holder.fotos.setAdapter(homeImageAdapter);
    }

    @Override
    public int getItemCount() {
        return home.size();
    }

}
