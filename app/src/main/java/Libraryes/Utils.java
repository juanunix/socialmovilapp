package Libraryes;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.iydsa.apps.challenge.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class Utils extends AppCompatActivity {
    private static Utils utils;
    private Context ctx;

    private Utils(Context context) {
        this.ctx = context;
    }

    public static Utils getInstance(Context ctx) {
        if (ctx != null) {
            utils = new Utils(ctx);
        }
        return utils;
    }

    public void LoadImage(int image, CircleImageView imagen) {
        try {
            Glide.with(ctx)
                    .load(image)
                    .placeholder(R.mipmap.ic_launcher)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadImageUrl(String image, CircleImageView imagen) {
        try {
            Glide.with(ctx)
                    .load(image)
                    .placeholder(R.mipmap.ic_launcher)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadImageUrl(String image, ImageView imagen) {
        try {
            Glide.with(ctx)
                    .load(image)
                    .placeholder(R.mipmap.ic_launcher)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoadImage(int image, ImageView imagen) {
        try {
            Glide.with(ctx)
                    .load(image)
                    .placeholder(R.mipmap.ic_launcher)
                    .fitCenter()
                    .into(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Mensaje(String mensaje) {
        Toast.makeText(ctx, mensaje, Toast.LENGTH_SHORT).show();
    }

    public void CustomMesaje(String mensaje, int Imagen) {
        Toast toast = new Toast(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.toast_custom, null);
        CircleImageView image = (CircleImageView) view.findViewById(R.id.imagen);
        LoadImage(Imagen, image);
        TextView text = (TextView) view.findViewById(R.id.mensaje);
        text.setText(mensaje);
        toast.setGravity(Gravity.BOTTOM, 0, 80);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }

    public void CustomMesaje(String mensaje, String url) {
        Toast toast = new Toast(ctx);
        View view = LayoutInflater.from(ctx).inflate(R.layout.toast_custom, null);
        CircleImageView image = (CircleImageView) view.findViewById(R.id.imagen);
        LoadImageUrl(url, image);
        TextView text = (TextView) view.findViewById(R.id.mensaje);
        text.setText(mensaje);
        toast.setGravity(Gravity.BOTTOM, 0, 80);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(view);
        toast.show();
    }
}
